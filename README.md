### Hi! I'm Jack Liu (Jianqiu Liu). 👋


- 🌱 Blog URL: **[wdft.com](https://wdft.com)**
- 💬 WeChat: **labsec**
- 📧 Email-1: **ljqlab@163.com**
- 📧 Email-2: **ljqlab@gmail.com**

gitlab.com/q89

<details>
<summary>WeChat QRcode</summary>

![labsec](https://raw.githubusercontent.com/ljq/ljq/main/wechat-ljq.png)

</details>

<!--
**ljq/ljq** is a ✨ _special_ ✨ repository because its `README.md` (this file) appears on your GitHub profile.

Here are some ideas to get you started:

- 🌱 I’m currently learning big data
- 👯 I’m looking to collaborate on 
- 🤔 I’m looking for help with ...
- 💬 Ask me about ...
- 📫 How to reach me: ...
- 😄 Pronouns: ...
- ⚡ Fun fact: ...
-->
